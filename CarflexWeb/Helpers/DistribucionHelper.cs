﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Globalization;
using Carflex.mx.Carflexweb.Controller;
using Carflex.mx.Carflexweb.Model;
using Carflex.mx.Promocion.Model;
using Carflex.mx.Catalogo.Model;
using Carflex.mx.Flota.Model;

namespace CarflexWeb.Helpers
{
 
    public static class DistribucionHelper
    {

        /// <summary>
        /// Helper para mostrar el slider
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="Filtro"></param>
        /// <returns>una lista de estructura html para el slider</returns>
        public static MvcHtmlString DistribucionSlider(this HtmlHelper helper, Carflex.mx.Catalogo.Model.FiltroVM Filtro)
        {

            CarflexwebController cPlantilla = new CarflexwebController();
            List<PromocionWebVM> lsvPromocionDistribucion = new List<PromocionWebVM>();
            string htmlString_helper = "";
            string activeCss = "";
            int active = 0;
            string[] img = new string[3];

            lsvPromocionDistribucion = cPlantilla.Distribucion(Filtro);

            foreach (PromocionWebVM opromocion in lsvPromocionDistribucion)
            {

                if (active == 0)
                    activeCss = "active";
                else
                    activeCss = "";

                if (opromocion.Imagenes.Count > 2)
                {

                    for (int i = 0; i < 3; i++)
                    {
                        if (i == 0)
                            img[i] = opromocion.Imagenes[i].Url;
                        if (i == 1)
                            img[i] = "<img src='" + opromocion.Imagenes[i].Url + "' alt='" + opromocion.Imagenes[i].Descripcion + "' id='" + opromocion.Imagenes[i].ImageId + "' class='img-responsive'>";
                        if (i == 2)
                            img[i] = "<img src='" + opromocion.Imagenes[i].Url + "' alt='" + opromocion.Imagenes[i].Descripcion + "' id='" + opromocion.Imagenes[i].ImageId + "'>";
                    }
                }

                if (img != null)
                {
                    htmlString_helper += String.Format(@"<div class='item " + activeCss + @"' style='background: url({5}) center top;'>
                                                        <div class='container'>
                                                            <div class='row'>
                                                                <a href='{0}'>
                                                                    <div class='col-md-4 col-sm-6 col-xs-6 col-md-offset-4'>
                                                                        <div class='carousel-content'>
                                                                            <h1>{1}</h1>
                                                                            <h3>{2}</h3>
                                                                            <h3 class='margin-slider'>{3}</h3>
                                                                            <h1 class='price-promo-slider'>{4}</h1>
                                                                            <br />
                                                                            {7}
                                                                        </div>
                                                                    </div>
                                                                    <div class='col-md-4 col-sm-6 col-xs-6 img-car'>
                                                                        <div class='carousel-content'>
                                                                            {6}
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>", opromocion.Url, opromocion.Titulo, opromocion.Subtitulo, opromocion.Subtitulo2, opromocion.Precio, img[0], img[1], img[2]);

                }
                else
                {
                    htmlString_helper += String.Format(@"<div class='item " + activeCss + @"' style='background: url() center top;'>
                                                        <div class='container'>
                                                            <div class='row'>
                                                                <a href='{0}'>
                                                                    <div class='col-md-4 col-sm-6 col-xs-6 col-md-offset-4'>
                                                                        <div class='carousel-content'>
                                                                            <h1>{1}</h1>
                                                                            <h3>{2}</h3>
                                                                            <h3 class='margin-slider'>{3}</h3>
                                                                            <h1 class='price-promo-slider'>{4}</h1>
                                                                            <br />
                                                                            <img src=''>
                                                                        </div>
                                                                    </div>
                                                                    <div class='col-md-4 col-sm-6 col-xs-6 img-car'>
                                                                        <div class='carousel-content'>
                                                                            <img src='' class='img-responsive'>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>", opromocion.Url, opromocion.Titulo, opromocion.Subtitulo, opromocion.Subtitulo2, opromocion.Precio);

                }


                active++;
            }

            return new MvcHtmlString(htmlString_helper);
        }

        /// <summary>
        /// Helper, realiza una peticion a dll CarflexWeb
        /// este trae todas las promociones de acuerdo a los filtros enviados
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="Plantilla"></param>
        /// <param name="Area"></param>
        /// <param name="Filtro"></param>
        /// <returns></returns>
        public static MvcHtmlString DistribucionGrupo(this HtmlHelper helper, Carflex.mx.Catalogo.Model.FiltroVM Filtro)
        {


            CarflexwebController cPlantilla = new CarflexwebController();

            List<PromocionWebVM> lsvPromocionDistribucion = new List<PromocionWebVM>();
           // PromocionWebVM vPromocionDistribucion = new PromocionWebVM();

            lsvPromocionDistribucion = cPlantilla.Distribucion(Filtro);

            string htmlString_helper = "";

            foreach (PromocionWebVM vPromocionDistribucion in lsvPromocionDistribucion)
            {
                
            htmlString_helper += String.Format(@"<div class='col-md-3 col-sm-6 border-Der'>
                    <div class='thumbnail'>
                        <h4 class='text-center'><a href=''>{0}</a></h4>", vPromocionDistribucion.Titulo);

            foreach (ImagenVM imagen in vPromocionDistribucion.Imagenes)
            {

                htmlString_helper += String.Format(@"
                                    <a href='{0}'><img src='{0}' class='img-thumbnail center-block' alt='{1}' id={2} ></a>
                                    ", imagen.Url, imagen.Descripcion, imagen.ImageId);

            }

            htmlString_helper += String.Format(@"
                                    <div class='caption'>
                                        <h5><a href=''>{0}</a></h5>
                                        <p>{1}</p>
                                    </div>
                                </div>
                           </div>", vPromocionDistribucion.Descripcion, vPromocionDistribucion.Resumen);
            }
            
            return new MvcHtmlString(htmlString_helper);
        }

       
        /// <summary>
        /// Helper que devuelve un auto en promocion de manera aleatoria en caso de que existan n autos
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="Filtro"></param>
        /// <returns>Retorna la estructura HTML de un auto en promocion</returns>
        public static MvcHtmlString DistribucionAuto(this HtmlHelper helper, Carflex.mx.Catalogo.Model.FiltroVM Filtro)
        {

            CarflexwebController cPlantilla = new CarflexwebController();

            List<PromocionWebVM> lsvPromocionDistribucion = new List<PromocionWebVM>();
           // PromocionWebVM vPromocionDistribucion = new PromocionWebVM();
            //AutoPromocionVM oPromocion = new AutoPromocionVM();
            string htmlString_helper = "";
            string Titulo = "";

            lsvPromocionDistribucion = cPlantilla.Distribucion(Filtro);

            
            
            foreach(PromocionWebVM vPromocionDistribucion in lsvPromocionDistribucion){

                Titulo = vPromocionDistribucion.Titulo;

                foreach (AutoPromocionVM oPromocion in vPromocionDistribucion.Autos)
                {
                    

                    htmlString_helper += String.Format(@"<div class='col-md-3 col-sm-6 col-xs-6'>
                    <div class='thumbnail' style='padding: 0px;'>
                        <a href='#'><img src='{0}' class='img-thumbnail' alt='...'></a>
                        ", oPromocion.ImagenAuto);

                    htmlString_helper += String.Format(@"<div class='estado-Promo enPromocion'>
                            <h5 style='color: #fff;'>{0}</h5>
                        </div>
                        ", Titulo);

                    htmlString_helper += String.Format(@"<div class='caption'>
                            <div class='modelo-Promo'>
                                <h4><a href='#' title='{0}'>{0}</a></h4>
                                <p>{1} - {2}</p>
                            </div>
                            <div class='precios-Promo'>
                                <p class='text-center'><del>{3}</del></p>
                                <h4 class='text-center'><a href='#' class='promoPrecio'>Ahora: {4}</a></h4>
                            </div>
                        </div>
                    </div>
                </div>", oPromocion.Fabricante + " " + oPromocion.Marca + " " + oPromocion.modelo, oPromocion.Transmision, oPromocion.Ciudad, Convert.ToDecimal(oPromocion.PrecioContado).ToString("C0"), Convert.ToDecimal(oPromocion.DescuentoNominal).ToString("C0"));

                }

            }

            return new MvcHtmlString(htmlString_helper);
        }

        /// <summary>
        /// Helper que devuelve un auto en promocion de manera aleatoria en caso de que existan n autos css remate
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="Filtro"></param>
        /// <returns>Retorna la estructura HTML de un auto en promocion</returns>
        public static MvcHtmlString DistribucionAutoRemate(this HtmlHelper helper, Carflex.mx.Catalogo.Model.FiltroVM Filtro)
        {

            CarflexwebController cPlantilla = new CarflexwebController();

            List<PromocionWebVM> lsvPromocionDistribucion = new List<PromocionWebVM>();
            // PromocionWebVM vPromocionDistribucion = new PromocionWebVM();
            //AutoPromocionVM oPromocion = new AutoPromocionVM();
            string htmlString_helper = "";
            string Titulo = "";

            lsvPromocionDistribucion = cPlantilla.Distribucion(Filtro);



            foreach (PromocionWebVM vPromocionDistribucion in lsvPromocionDistribucion)
            {

                Titulo = vPromocionDistribucion.Titulo;

                foreach (AutoPromocionVM oPromocion in vPromocionDistribucion.Autos)
                {


                    htmlString_helper += String.Format(@"<div class='col-md-3 col-sm-6 col-xs-6'>
                    <div class='thumbnail' style='padding: 0px;'>
                        <a href='#'><img src='{0}' class='img-thumbnail' alt='...'></a>
                        ", oPromocion.ImagenAuto);

                    htmlString_helper += String.Format(@"<div class='estado-Promo enRemate'>
                            <h5 style='color: #fff;'>{0}</h5>
                        </div>
                        ", Titulo);

                    htmlString_helper += String.Format(@"<div class='caption'>
                            <div class='modelo-Promo'>
                                <h4><a href='#' title='{0}'>{0}</a></h4>
                                <p>{1} - {2}</p>
                            </div>
                            <div class='precios-Promo'>
                                <p class='text-center'><del>{3}</del></p>
                                <h4 class='text-center'><a href='#' class='promoPrecio'>Ahora: {4}</a></h4>
                            </div>
                        </div>
                    </div>
                </div>", oPromocion.Fabricante + " " + oPromocion.Marca + " " + oPromocion.modelo, oPromocion.Transmision, oPromocion.Ciudad, Convert.ToDecimal(oPromocion.PrecioContado).ToString("C0"), Convert.ToDecimal(oPromocion.DescuentoNominal).ToString("C0"));

                }

            }

            return new MvcHtmlString(htmlString_helper);

        }

        /// <summary>
        /// Helper que devuelve un auto en promocion de manera aleatoria en caso de que existan n autos css remate
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="Filtro"></param>
        /// <returns>Retorna la estructura HTML de un auto en promocion</returns>
        public static MvcHtmlString DistribucionSliderVenta(this HtmlHelper helper, Carflex.mx.Catalogo.Model.FiltroVM Filtro)
        {

            CarflexwebController cPlantilla = new CarflexwebController();

            List<PromocionWebVM> lsvPromocionDistribucion = new List<PromocionWebVM>();
            // PromocionWebVM vPromocionDistribucion = new PromocionWebVM();
            //AutoPromocionVM oPromocion = new AutoPromocionVM();
            string htmlString_helper = "";
            string Titulo = "";

            lsvPromocionDistribucion = cPlantilla.Distribucion(Filtro);

            htmlString_helper = @"<div class='jcarousel-wrapper'>
	                                <div class='jcarousel'>
		                                <ul>";
			           
			
		   
            foreach (PromocionWebVM vPromocionDistribucion in lsvPromocionDistribucion)
            {

                Titulo = vPromocionDistribucion.Titulo;

                foreach (AutoPromocionVM oPromocion in vPromocionDistribucion.Autos)
                {

                    //**********************************************************************//
                    //******
                    //******
                    //**********************************************************************//
                    htmlString_helper += String.Format(@"<li>
				            <div class='jcarousel-content'>
					            <div class='ribbon-top text-center'>AHORRA $ 4,000</div>
					            <a href='#'><img src='{0}' alt='Image 1'></a>
					            <h4><a href='#'>{1}</a></h4>
					            <p>A, A/C - PLATA 2012</p>
					            <p><del>$190,000</del></p>
					            <h4 class='text-rojo'>$156,000</h4>
				            </div>
			            </li>", oPromocion.ImagenAuto, oPromocion.Fabricante + " " + oPromocion.Marca + " " + oPromocion.modelo, oPromocion.Transmision, oPromocion.Ciudad, Convert.ToDecimal(oPromocion.PrecioContado).ToString("C0"), Convert.ToDecimal(oPromocion.DescuentoNominal).ToString("C0"));

                    //*********************************************************************//
                    //******
                    //******
                    //*********************************************************************//
                }

            }

            htmlString_helper = @"</ul>
	                        </div>
	                        <a href='#' class='jcarousel-control-prev'><span class='fa fa-caret-left'></span></a>
	                        <a href='#' class='jcarousel-control-next'><span class='fa fa-caret-right'></span></a>
                        </div>";


            return new MvcHtmlString(htmlString_helper);
        }
    }
}