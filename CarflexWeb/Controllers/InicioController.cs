﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Carflex.mx.Flota.Services;
using Carflex.mx.Flota.Model;
using Carflex.mx.Catalogo.Services;
using carflex.mx.socio.Services;
using Carflex.mx.Carflexweb.Services;
using Carflex.mx.Carflexweb.Model;

namespace CarflexWeb.Controllers
{
    public class InicioController : Controller
    {
        //
        List<SelectListItem> limarcas = new List<SelectListItem>();
        List<SelectListItem> lilocalidades = new List<SelectListItem>();
        CarflexwebServices cargar = new CarflexwebServices();
        Configuracion oConfiguracion = new Configuracion();

        int SocioId = 1;


        // GET: /Inicio/

        public ActionResult Index()
        {
            limarcas.Add(new SelectListItem { Text = "Todas", Value = "0" });
            lilocalidades.Add(new SelectListItem { Text = "Todas", Value = "0" });
            var localidades = cargar.GetCities(SocioId);
            localidades.ForEach(x => lilocalidades.Add(new SelectListItem { Text = x.Nombre, Value = x.CiudadId.ToString() }));

            List<MarcaVM> lsMarca = new List<MarcaVM>();
            lsMarca = cargar.GetBrands(SocioId);
            foreach (var x in lsMarca)
            {
                limarcas.Add(new SelectListItem
                {
                    Text = x.Nombre,
                    Value = x.MarcaId.ToString(),
                });
            }

            Carflex.mx.Catalogo.Model.FiltroVM oFiltro = new Carflex.mx.Catalogo.Model.FiltroVM();
            Carflex.mx.Catalogo.Model.FiltroVM oFiltro1 = new Carflex.mx.Catalogo.Model.FiltroVM();
            Carflex.mx.Catalogo.Model.FiltroVM oFiltro2 = new Carflex.mx.Catalogo.Model.FiltroVM();
            Carflex.mx.Catalogo.Model.FiltroVM oFiltro3 = new Carflex.mx.Catalogo.Model.FiltroVM();

            Carflex.mx.Catalogo.Model.FiltroVM oFiltroSlider = new Carflex.mx.Catalogo.Model.FiltroVM();
            Carflex.mx.Catalogo.Model.FiltroVM oFiltroFlota = new Carflex.mx.Catalogo.Model.FiltroVM();
            Carflex.mx.Catalogo.Model.FiltroVM oFiltroFlota2 = new Carflex.mx.Catalogo.Model.FiltroVM();
            Carflex.mx.Catalogo.Model.FiltroVM oFiltroFlota3 = new Carflex.mx.Catalogo.Model.FiltroVM();
            Carflex.mx.Catalogo.Model.FiltroVM oFiltroFlota4 = new Carflex.mx.Catalogo.Model.FiltroVM();

            
            //en una pagina solo puede haber una plantilla

            oFiltroSlider.SocioId = SocioId;
            oFiltroSlider.NombrePagina = "home.aspx";
            oFiltroSlider.NombreArea = "Slider";

            oFiltro.SocioId = SocioId;
            oFiltro.NombrePagina = "home.aspx";
            oFiltro.NombreArea = "Publicidad Home";

            oFiltro1.SocioId = SocioId;
            oFiltro1.NombrePagina = "home.aspx";
            oFiltro1.NombreArea = "Publicidad Home 2";

            oFiltro2.SocioId = SocioId;
            oFiltro2.NombrePagina = "home.aspx";
            oFiltro2.NombreArea = "Publicidad Home 3";

            oFiltro3.SocioId = SocioId;
            oFiltro3.NombrePagina = "home.aspx";
            oFiltro3.NombreArea = "Publicidad Home 4";


            oFiltroFlota.SocioId = SocioId;
            oFiltroFlota.NombrePagina = "home.aspx";
            oFiltroFlota.NombreArea = "Home flota 1";


            oFiltroFlota2.SocioId = SocioId;
            oFiltroFlota2.NombrePagina = "home.aspx";
            oFiltroFlota2.NombreArea = "Home flota 2";


            oFiltroFlota3.SocioId = SocioId;
            oFiltroFlota3.NombrePagina = "home.aspx";
            oFiltroFlota3.NombreArea = "Home flota 3";


            oFiltroFlota4.SocioId = SocioId;
            oFiltroFlota4.NombrePagina = "home.aspx";
            oFiltroFlota4.NombreArea = "Home flota 4";

            ViewData["lsFiltro"] = oFiltro;
            ViewData["lsFiltro1"] = oFiltro1;
            ViewData["lsFiltro2"] = oFiltro2;
            ViewData["lsFiltro3"] = oFiltro3;
            ViewData["FiltroSlider"] = oFiltroSlider;
            ViewData["FiltroFlota"] = oFiltroFlota;
            ViewData["FiltroFlota2"] = oFiltroFlota2;
            ViewData["FiltroFlota3"] = oFiltroFlota3;
            ViewData["FiltroFlota4"] = oFiltroFlota4;

            oConfiguracion = cargar.GetConfigurationPriceSlider(1);

            ViewBag.Config = oConfiguracion;
            ViewData["lsmarcas"] = limarcas;
            ViewData["lslocalidades"] = lilocalidades;
            ViewData["lsmodelos"] = "Todas";
            ViewData["mva"] = "";
            ViewData["precioinicial"] = 0;
            ViewData["preciofinal"] = 60000;

            return View();
        }

        public ActionResult CargaCombosByMarca(string id)
        {
            int MarcaId = Convert.ToInt32(id);
            List<string> lsModel = cargar.GetModels(MarcaId);

            if (HttpContext.Request.IsAjaxRequest())
            {
                return Json(lsModel, JsonRequestBehavior.AllowGet);
            }

            return RedirectToAction("Index");
        }

    }
}
